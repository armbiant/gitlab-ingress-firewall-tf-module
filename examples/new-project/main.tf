# [terraform-project]/main.tf

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 4.0"
    }
  }
  required_version = ">= 1.0"
}

# Define the Google Cloud Provider
provider "google" {
  project = var.gcp_project
}

# GCP VPC network for environment resources
data "google_compute_network" "vpc_network" {
  name = "default"
}

# Create an ingress firewall allow rule
module "ingress_allow_rule" {
  source = "git@gitlab.com:gitlab-com/infra-standards/terraform-modules/gcp/gcp-ingress-firewall-tf-module.git?ref=2.5.10"

  # Required Variables
  firewall_description = "Creates firewall rule targeting tagged instances"
  firewall_name        = "test-firewall"
  gcp_network          = data.google_compute_network.vpc_network.self_link
  gcp_project          = var.gcp_project
  target_tags          = ["web-lb"]

  # Optional Variables
  rules_allow = [
    {
      protocol = "tcp"
      ports    = ["443"]
    }
  ]
  source_ranges = var.cloudflare_ips
  # source_tags   = ["testing", "testing2"]
}

# Create an ingress firewall deny rule
module "ingress_deny_rule" {
  source = "git@gitlab.com:gitlab-com/infra-standards/terraform-modules/gcp/gcp-ingress-firewall-tf-module.git?ref=2.5.10"

  # Required Variables
  firewall_description = "Deny all ingress firewall rule"
  firewall_name        = "deny-all-rule"
  gcp_network          = data.google_compute_network.vpc_network.self_link
  gcp_project          = var.gcp_project
  source_ranges        = ["0.0.0.0/0"]
  target_tags          = ["web-lb"]

  # Optional Variables
  firewall_disabled = "false"
  firewall_priority = "65000"
  rules_deny = [
    {
      protocol = "all"
      ports    = ["0-65535"]
    }
  ]
}
